﻿using Umbraco.Core.Models;

namespace DetangledDigital.UHydrator.Models
{
    public interface IBaseModel
    {
        int Id { get; set; }
        string Name { get; set; }
        string Path { get; set; }
        string Url { get; set; }
        IPublishedContent PublishedContent { get; set; }
    }
}