﻿using Umbraco.Core.Models;

namespace DetangledDigital.UHydrator.Models
{
    public abstract class BaseModel : IBaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public IPublishedContent PublishedContent { get; set; }
    }
}