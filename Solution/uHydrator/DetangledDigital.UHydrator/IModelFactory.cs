﻿namespace DetangledDigital.UHydrator
{
    using Umbraco.Core.Models;
    using DetangledDigital.UHydrator.Models;

    public interface IModelFactory
    {
        T Create<T>(IPublishedContent content) where T : IBaseModel, new();
        T Set<T>(T viewModel, IPublishedContent content) where T : IBaseModel;
    }
}