﻿namespace DetangledDigital.UHydrator.Services
{
    using DetangledDigital.UHydrator.Models;
    using Umbraco.Core.Models;

    internal interface IHydrator
    {
        T HydrateModel<T>(T model, IPublishedContent publishedContent) where T : IBaseModel;
    }
}