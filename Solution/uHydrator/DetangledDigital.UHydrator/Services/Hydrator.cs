﻿using Our.Umbraco.PropertyConverters.Models;
using Umbraco.Web;

namespace DetangledDigital.UHydrator.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Attributes;
    using Models;
    using Umbraco.Core.Models;

    internal class Hydrator : IHydrator
    {
        public T HydrateModel<T>(T model, IPublishedContent publishedContent) where T : IBaseModel
        {
            // Default CMS properties
            model.Id = publishedContent.Id;
            model.Name = publishedContent.Name;
            model.Path = publishedContent.Path;
            model.Url = publishedContent.Url;
            model.PublishedContent = publishedContent;

            // Hydrate UmbracoProperties in model
            HydrateUmbracoProperties(model, publishedContent);

            // Hydrate any composed mixins
            HydrateUmbracoMixins(model, publishedContent);

            return model;
        }

        private static void HydrateUmbracoMixins<T>(T viewModel, IPublishedContent publishedContent)
        {
            var propertiesToHydrate = viewModel
                .GetType()
                .GetProperties()
                .Where(p => p.GetCustomAttributes(typeof (UmbracoCompositeAttribute), true).Any());

            foreach (var property in propertiesToHydrate)
            {
                Type type = property.PropertyType;
                var propertyValue = Activator.CreateInstance(type);

                HydrateUmbracoProperties(propertyValue, publishedContent);
                property.SetValue(viewModel, propertyValue);
            }
        }

        private static void HydrateUmbracoProperties<T>(T model, IPublishedContent publishedContent)
        {
            var propertiesToHydrate = model
                .GetType()
                .GetProperties()
                .Where(p => p.GetCustomAttributes(typeof (UmbracoPropertyAttribute), true).Any());

            foreach (var property in propertiesToHydrate)
            {
                var umbracoPropertyAttribute = property.GetCustomAttribute<UmbracoPropertyAttribute>();
                if (umbracoPropertyAttribute == null) continue;

                // See if the render model contains the property
                var cmsProperty = publishedContent.Properties.FirstOrDefault(x => x.PropertyTypeAlias == umbracoPropertyAttribute.PropertyAlias);
                if (cmsProperty == null || !cmsProperty.HasValue || cmsProperty.Value == null) continue;
             
                // Parse to string for starters
                var propertyValue = cmsProperty.Value.ToString();

                if (string.IsNullOrWhiteSpace(propertyValue)) continue;

                // Bool
                if (property.PropertyType == typeof (bool))
                {
                    property.SetValue(model, publishedContent.GetPropertyValue<bool>(umbracoPropertyAttribute.PropertyAlias), null);
                    continue;
                }

                // List<int>
                if (property.PropertyType == typeof (List<int>))
                {
                    property.SetValue(model, propertyValue.Split(',').Select(int.Parse).ToList());
                    continue;
                }

                // List<string>
                if (property.PropertyType == typeof (List<string>))
                {
                    property.SetValue(model, propertyValue.Split(',').ToList());
                    continue;
                }

                // IPublishedContent
                if (property.PropertyType == typeof(IPublishedContent))
                {
                    property.SetValue(model, publishedContent.GetPropertyValue<IPublishedContent>(umbracoPropertyAttribute.PropertyAlias));
                    continue;
                }

                // IEnumerable<IPublishedContent>
                if (property.PropertyType == typeof(IEnumerable<IPublishedContent>))
                {
                    var nodes = publishedContent.GetPropertyValue<IEnumerable<IPublishedContent>>(umbracoPropertyAttribute.PropertyAlias);
                    property.SetValue(model, nodes);
                    continue;
                }

                // RelatedLinks
                if (property.PropertyType == typeof(RelatedLinks))
                {
                    var nodes = publishedContent.GetPropertyValue<RelatedLinks>(umbracoPropertyAttribute.PropertyAlias);
                    property.SetValue(model, nodes);
                    continue;
                }

                // DateTime
                if (property.PropertyType == typeof(DateTime))
                {
                    property.SetValue(model, publishedContent.GetPropertyValue<DateTime>(umbracoPropertyAttribute.PropertyAlias));
                    continue;
                }

                // All other types
                var parsedPropertyValue = Convert.ChangeType(propertyValue, property.PropertyType);
                if (parsedPropertyValue == null) continue;

                property.SetValue(model, parsedPropertyValue);
            }
        }
    }
}