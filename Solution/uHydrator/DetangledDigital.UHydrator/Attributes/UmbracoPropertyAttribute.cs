﻿namespace DetangledDigital.UHydrator.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public class UmbracoPropertyAttribute : Attribute
    {
        public readonly string PropertyAlias;
        public readonly PropertyEditor PropertyEditor;

        public UmbracoPropertyAttribute(string propertyAlias, PropertyEditor propertyEditor = PropertyEditor.Textbox)
        {
            if (propertyAlias == null) throw new ArgumentNullException("propertyAlias");

            PropertyAlias = propertyAlias;
            PropertyEditor = propertyEditor;
        }
    }

    public enum PropertyEditor
    {
        CheckBoxList,
        ColorPicker,
        ContentPicker,
        Date,
        DateTime,
        DictionaryPicker,
        DropDownList,
        DropDownListMultiple,
        DropdownlistMultiplePublishKeys,
        DropdownlistPublishingKey,
        FolderBrowser,
        ImageCropper,
        Integer,
        MacroContainer,
        MediaPicker,
        MemberPicker,
        MultiNodeTreePicker,
        MultipleTextstring,
        NoEdit,
        PickerRelations,
        RadioButtonList,
        RelatedLinks,
        Slider,
        Tags,
        Textbox,
        TextboxMultiple,
        TinyMCEv3,
        TinyMCE,
        TrueFalse,
        UltimatePicker,
        UltraSimpleEditor,
        UmbracoUserControlWrapper,
        UploadField,
        XPathCheckBoxList,
        XPathDropDownList
    }
}