﻿using DetangledDigital.UHydrator.Attributes;
using DetangledDigital.UHydrator.Models;

namespace uHydratorExample.UI.Models
{
    public class ContentMaster : BaseModel
    {
        [UmbracoProperty("pageTitle")]
        public string PageTitle { get; set; }

        [UmbracoProperty("shortDescription", PropertyEditor.UltraSimpleEditor)]
        public string ShortDescription { get; set; }
    }
}